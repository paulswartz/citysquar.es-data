#!/usr/bin/env python3
import json
import sys
import os
import shutil

if len(sys.argv) < 2:
    print("Usage: {0} <path to .geojson file>".format(sys.argv[0]))
    sys.exit(1)

FILENAME = sys.argv[1]
BASENAME = os.path.basename(FILENAME)
ROOT = os.path.splitext(BASENAME)[0]

if os.path.exists(ROOT):
    shutil.rmtree(ROOT)
os.mkdir(ROOT)

with open(FILENAME) as f:
    geojson = json.load(f)
for feature in geojson["features"]:
    id_ = feature["id"]
    properties = feature.pop("properties", {})
    with open(os.path.join(ROOT, "{0}.json".format(id_)), 'w') as feature_file:
        json.dump(feature, feature_file)

        if properties:
            prop_base = os.path.join(ROOT, str(id_))
            os.mkdir(prop_base)

            for prop, value in properties.items():
                with open(os.path.join(prop_base, "{0}.json".format(prop)),
                          'w') as prop_file:
                    json.dump(value, prop_file)
