#!/usr/bin/env python3
import json
import sys
import os
import glob

if len(sys.argv) < 2:
    print("Usage: {0} <path to root of flattened data>".format(sys.argv[0]))
    sys.exit(1)

ROOT = sys.argv[1]

def feature_for_filename(filename):
    with open(filename) as f:
        feature = json.load(f)

    feature['properties'] = properties = {}
    property_base = os.path.join(ROOT, str(feature["id"]), "*.json")
    for property_filename in glob.glob(property_base):
        prop = os.path.splitext(os.path.basename(property_filename))[0]
        with open(property_filename) as prop_file:
            properties[prop] = json.load(prop_file)

    return feature
features = [
    feature_for_filename(filename)
    for filename in glob.glob(os.path.join(ROOT, "*.json"))]

with open("{0}.geojson".format(ROOT), "w") as output:
    json.dump({"type": "FeatureCollection",
               "features": features}, output)
