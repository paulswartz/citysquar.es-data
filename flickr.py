#!/usr/bin/env python3

# Add an ImageURL, ImageAttribution, and ImageAttributionURL from Flickr

import glob
import sys
import os
import json
import requests
import bs4
from urllib.parse import urljoin

URL="http://www.flickr.com/search/?q=Richard%20A.%20Umanzio%20Square&l=cc&ct=0&mt=all&adv=1"
QUERY = {
    "l": "cc",
    "ct": "0",
    "mt": "all",
    "adv": "1"
}

if len(sys.argv) < 2:
    print("Usage: {0} <path to root of flattened data>".format(sys.argv[0]))
    sys.exit(1)

def handler(filename):
    with open(filename) as f:
        name = json.load(f)
    r = requests.get(URL, params=dict(QUERY, q=name))
    bs = bs4.BeautifulSoup(r.content)
    items = bs.find(id="photo-display-container").find(
        "div", class_="photo-display-item")
    if not items:
        return False
    img_url = items.find('img')['data-defer-src']
    owner = items.find('a', class_='owner')
    owner_name = owner['title']
    owner_url = urljoin(URL, owner['href'])
    data = {
        "ImageURL": img_url,
        "ImageAttribution": owner_name,
        "ImageAttributionURL": owner_url
    }
    property_base = os.path.dirname(filename)
    for prop, value in data.items():
        with open(os.path.join(property_base, "{0}.json".format(prop)),
                  'w') as prop_file:
            json.dump(value, prop_file)
    return True

if __name__ == "__main__":
    import multiprocessing.pool
    pool = multiprocessing.pool.Pool()
    pool.map(handler,
             glob.glob(os.path.join(sys.argv[1], "*", "Name.json")))
    pool.close()
    pool.join()
