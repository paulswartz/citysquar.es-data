#!/usr/bin/env python3

# Clean up the names (convert Sq and Sq. to Square)

import glob
import sys
import os
import json
import re

REPLACEMENTS = {
    r"Sq\b\.?": "Square",
    r"Cir\b": "Circle",
    r"Rd\b": "Road",
    r"St\b": "Street",
    r"Ct\b": "Court"
}

if len(sys.argv) < 2:
    print("Usage: {0} <path to root of flattened data>".format(sys.argv[0]))
    sys.exit(1)

for filename in glob.glob(os.path.join(sys.argv[1], "*", "Name.json")):
    with open(filename) as f:
        name = json.load(f)
    for pattern, replacement in REPLACEMENTS.items():
        name = re.sub(pattern, replacement, name)
    with open(filename, 'w') as f:
        json.dump(name, f)
